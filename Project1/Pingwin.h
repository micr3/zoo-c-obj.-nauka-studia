#pragma once
#include "Zwierze.h"
#include <string>

using namespace std;

class Pingwin :
	public Zwierze
{
protected:
	float Dlugosc_dzioba;
public:
	virtual string Odglos();
	void SetDlugosc_dzioba(float nowaDlugosc);
	float GetDlugosc_dzioba();
	Pingwin();
	Pingwin(string NowaNazwa);
	virtual ~Pingwin();
};

