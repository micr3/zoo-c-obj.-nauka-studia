#pragma once

#include "Zwierze.h"
#include <string>

using namespace std;

class Slon : public Zwierze
{
protected:
	float Dlugosc_traby;

public:
	virtual string Odglos();
	Slon();
	void SetDlugosc_traby(float nowaDlugosc);
	float GetDlugosc_traby();
	Slon(string NowaNazwa);
	virtual ~Slon();


};

