#pragma once
#include <string>

using namespace std;



class Zwierze
{
	
protected:
	int Liczba_nog;
	string Nazwa;
public:
	virtual string Odglos(); //=0 zmiana jednej metody na virtualna tworzy ca�� klase abstrakcyjna xD
	void SetLiczba_nog(int nowaLiczba);
	int GetLiczba_nog();
	string GetNazwa();
	void SetNazwa(string NowaNazwa);
	virtual string Opis();
	Zwierze();
	Zwierze(string NowaNazwa, int nowaLiczba);
	virtual ~Zwierze();
};

