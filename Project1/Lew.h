#pragma once
#include <string>
#include "Zwierze.h"
#include "Slon.h"
using namespace std;

class Lew : public Zwierze
{
	public:
	virtual string Odglos();
	bool Pozywienie(Zwierze *zw);
	Lew();
	Lew(string NowaNazwa);
	virtual ~Lew();


};